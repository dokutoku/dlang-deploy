/**
 * CUI Hello
 *
 * License: CC0 1.0 Universal
 */
module dlang_hello.console;


version (Console):

private import std;
private import std.stdio;

void main()

	do
	{
		static import std.stdio;

		std.stdio.stdout.writeln("Hello, World!");
	}
