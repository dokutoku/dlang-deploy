/**
 * Windows DLL sample
 *
 * License: CC0 1.0 Universal
 */
module dlang_hello.sys.windows.dll;


version (DynamicLibrary):
version (Windows):

private static import core.runtime;
private static import core.sys.windows.dll;
private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import core.stdc.stdio;

extern (Windows)
core.sys.windows.windef.BOOL DllMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.DWORD ulReason, core.sys.windows.winnt.LPVOID pvReserved)

	do
	{
		static import core.runtime;
		static import core.sys.windows.dll;
		static import core.sys.windows.windef;
		static import core.sys.windows.winnt;

		switch (ulReason) {
			case core.sys.windows.winnt.DLL_PROCESS_ATTACH:
				core.runtime.Runtime.initialize();

				break;

			case core.sys.windows.winnt.DLL_PROCESS_DETACH:
				core.runtime.Runtime.terminate();

				break;

			case core.sys.windows.winnt.DLL_THREAD_ATTACH:
				core.sys.windows.dll.dll_thread_attach(true, true);

				break;

			case core.sys.windows.winnt.DLL_THREAD_DETACH:
				core.sys.windows.dll.dll_thread_detach(true, true);

				break;

			default:
				break;
		}

		return core.sys.windows.windef.TRUE;
	}

extern(C)
nothrow @nogc
export void dllprint()

	do
	{
		static import core.stdc.stdio;

		core.stdc.stdio.printf("hello dll world\n");
	}
