/**
 * GUI Hello
 *
 * License: CC0 1.0 Universal
 */
module dlang_hello.gui;


version (GUI):

import dlangui;

mixin APP_ENTRY_POINT;

extern (C)
int UIAppMain(string[] args)

	do
	{
		Window window = Platform.instance.createWindow("Hello, World!", null);
		window.mainWidget = (new Button()).text("Hello, World!"d).margins(Rect(20, 20, 20, 20));
		window.show();

		return Platform.instance.enterMessageLoop();
	}
