[![pipeline status](https://gitlab.com/dokutoku/dlang-deploy/badges/master/pipeline.svg)](https://gitlab.com/dokutoku/dlang-deploy/commits/master)
# dlang-deploy

Sample to deploy D language libraries and software

## Suppported platfom
- Windows 32bit
- Windows 64bit
- wasm

## Output Files
### Windows
#### 32bit
- https://dokutoku.gitlab.io/dlang-deploy/test_CUI_x86.exe
- https://dokutoku.gitlab.io/dlang-deploy/test_x86.dll

#### 64bit
- https://dokutoku.gitlab.io/dlang-deploy/test_CUI_x64.exe
- https://dokutoku.gitlab.io/dlang-deploy/test_x64.dll

### Linux
#### 64bit
- https://dokutoku.gitlab.io/dlang-deploy/test_linux_CUI_x64
- https://dokutoku.gitlab.io/dlang-deploy/test_linux_GUI_x64

### wasm
- https://dokutoku.gitlab.io/dlang-deploy/test.wasm

### Checksums
- https://dokutoku.gitlab.io/dlang-deploy/SHA256SUMS
- https://dokutoku.gitlab.io/dlang-deploy/SHA512SUMS

## License
[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
