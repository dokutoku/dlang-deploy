#!/bin/sh

# Linux x64

ldc_linux_x64_uri='https://github.com/ldc-developers/ldc/releases/download/v1.23.0/ldc2-1.23.0-linux-x86_64.tar.xz';
ldc_linux_x64_sha512_hash='b3c5d56645345849f0c505f1a6f9856027d5c5cc4bc0e583cae2794e1a43427b148bff225c9bfe0e683878a06618eaf179cfd4a8950a2c7ef147fa6e466a3c19';
ldc_linux_x64_base='ldc2-1.23.0-linux-x86_64';
ldc_linux_x64_cache_file='ldc2-linux-x86_64.tar.xz';
ldc_windows_multilib_uri='https://github.com/ldc-developers/ldc/releases/download/v1.23.0/ldc2-1.23.0-windows-multilib.7z';
ldc_windows_multilib_sha512_hash='1585793bbce8d64ff8fc4876fd43cf3025df13b4b4bb794f6c8b904909d9314d8e4a6de044e6db182c71cffff241c3ee6a6b3557435f679222cb88c71748c8e7';
ldc_windows_multilib_base='ldc2-1.23.0-windows-multilib';
ldc_windows_multilib_cache_file='ldc2-windows-multilib.7z';
ldc_osx_uri='https://github.com/ldc-developers/ldc/releases/download/v1.23.0/ldc2-1.23.0-osx-x86_64.tar.xz';
ldc_osx_sha512_hash='8b753c88ab0bdc1001dc949bebe64e18df22a4296ebcdb3fad6aed8c9c78d7c8563699a948d919eb3f0986737c6c7c33c2a93f481b46b55f1c38007eee08dfde';
ldc_osx_base='ldc2-1.23.0-osx-x86_64';
ldc_osx_cache_file='ldc2-osx-x86_64.xz';
ldc_linux_aarch64_uri='https://github.com/ldc-developers/ldc/releases/download/v1.23.0/ldc2-1.23.0-linux-aarch64.tar.xz';
ldc_linux_aarch64_sha512_hash='63b7bbce8b20b96578f40022a74636dbc062a49d303cea7539cfa1164568d03fec77ff44a04215dc884460edfb041aab9553a96be14f14bf911057bc96fe3f67';
ldc_linux_aarch64_base='ldc2-1.23.0-linux-aarch64';
ldc_linux_aarch64_cache_file='ldc2-aarch64.tar.xz';
ldc_android_aarch64_uri='https://github.com/ldc-developers/ldc/releases/download/v1.23.0/ldc2-1.23.0-android-aarch64.tar.xz';
ldc_android_aarch64_sha512_hash='a00b99133d38c1fa6255a1358704741202fa0acc21a433b3025fda80502cf71dfb33f9263a2a55b3da3a0dfdbc8bbc9df73595e9b6892ed0dcabc9309ea4a326';
ldc_android_aarch64_base='ldc2-1.23.0-android-aarch64';
ldc_android_aarch64_cache_file='ldc2-android-aarch64.tar.xz';
ldc_android_armv7a_uri='https://github.com/ldc-developers/ldc/releases/download/v1.23.0/ldc2-1.23.0-android-armv7a.tar.xz';
ldc_android_armv7a_sha512_hash='587ff54802eba112cdb8b0e1c33a75d45bc6000139ef626bdd6d7a4a1943a7448b3cc43624e0d5464f74161a3fb68e0df50f651881e8c5c6522e66613a6ed5c5';
ldc_android_armv7a_base='ldc2-1.23.0-android-armv7a';
ldc_android_armv7a_cache_file='ldc2-android-armv7a.tar.xz';

# See URI for argument identifier names.
# https://dlang.org/spec/version.html#predefined-versions
if [ $# -lt 1 ];  then
	echo 'The number of arguments must be at least one.';

	exit 1;
fi

if ! (type sha512sum >/dev/null 2>&1); then
	echo 'sha512sum not found';

	exit 1;
fi

if ! (type curl >/dev/null 2>&1); then
	echo 'curl not found';

	exit 1;
fi

if ! (type 7z >/dev/null 2>&1); then
	echo '7z not found';

	exit 1;
fi

# $1 = file path
# $2 = hash
hash_verify() {
    local hash=`sha512sum -b $1 | grep -o '^[a-zA-Z0-9]*'`;

    if [ $hash = $2 ];  then
        echo 'true';

        return 1;
    else
        echo 'false';

        return 0;
    fi
}

# $1 = download uri
# $2 = cache file path
# $3 = download file path
download_and_verify() {
    if [ -f $2 ];  then
        local result=`hash_verify $2 $3`;

        if [ $result = 'true' ];  then
            echo 'true';

            return 1;
        fi

        rm $2;
    fi

    curl -A '' -L $1 -o $2;
    local result=`hash_verify $2 $3`;

    if [ $result = 'true' ];  then
        echo 'true';

        return 1;
    else
        echo "error $1 hash.";

        exit 1;
    fi
}

if [ ! -d './cache' ]; then
    mkdir './cache';
fi

if [ ! -d './ldc2' ]; then
    mkdir './ldc2';
fi

echo 'ls ./cache';
echo;
ls './cache';
echo;

download_and_verify $ldc_linux_x64_uri './cache/'$ldc_linux_x64_cache_file $ldc_linux_x64_sha512_hash;
tar xf './cache/'$ldc_linux_x64_cache_file -C './';
mv -T $ldc_linux_x64_base './ldc2/';
echo;
'./ldc2/bin/ldc2' --version;
echo;
cp -f './ldc2-conf/linux-x64.conf' './ldc2/etc/ldc2.conf';

if [ $1 = 'Windows' ] || [ $1 = 'all' ];  then
    download_and_verify $ldc_windows_multilib_uri './cache/'$ldc_windows_multilib_cache_file $ldc_windows_multilib_sha512_hash;
    7z x -y './cache/'$ldc_windows_multilib_cache_file;
    mv $ldc_windows_multilib_base'/lib32' './ldc2/windows-lib32';
    mv $ldc_windows_multilib_base'/lib64' './ldc2/windows-lib64';
    rm -r $ldc_windows_multilib_base;
fi

if [ $1 = 'OSX' ] || [ $1 = 'all' ];  then
    download_and_verify $ldc_osx_uri './cache/'$ldc_osx_cache_file $ldc_osx_sha512_hash;
    tar xf './cache/'$ldc_osx_cache_file -C './';
    mv $ldc_osx_base'/lib' './ldc2/osx-lib64';
    rm -r $ldc_osx_base;
fi

if [ $1 = 'linux_AArch64' ] || [ $1 = 'all' ];  then
    download_and_verify $ldc_linux_aarch64_uri './cache/'$ldc_linux_aarch64_cache_file $ldc_linux_aarch64_sha512_hash;
    tar xf './cache/'$ldc_linux_aarch64_cache_file -C './';
    mv $ldc_linux_aarch64_base'/lib' './ldc2/aarch-lib64';
    rm -r $ldc_linux_aarch64_base;
fi

if [ $1 = 'Android_AArch64' ] || [ $1 = 'all' ];  then
    download_and_verify $ldc_android_aarch64_uri './cache/'$ldc_android_aarch64_cache_file $ldc_android_aarch64_sha512_hash;
    tar xf './cache/'$ldc_android_aarch64_cache_file -C './';
    mv $ldc_android_aarch64_base'/lib' './ldc2/andoird-aarch-lib64';
    rm -r $ldc_android_aarch64_base;
fi

if [ $1 = 'Android_armv7a' ] || [ $1 = 'all' ];  then
    download_and_verify $ldc_android_armv7a_uri './cache/'$ldc_android_armv7a_cache_file $ldc_android_armv7a_sha512_hash;
    tar xf './cache/'$ldc_android_armv7a_cache_file -C './';
    mv $ldc_android_armv7a_base'/lib' './ldc2/andoird-armv7a-lib64';
    rm -r $ldc_android_armv7a_base;
fi
